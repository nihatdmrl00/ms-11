package com.ingress.edu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ms11Application {

	public static void main(String[] args) {
		SpringApplication.run(Ms11Application.class, args);
	}

}
